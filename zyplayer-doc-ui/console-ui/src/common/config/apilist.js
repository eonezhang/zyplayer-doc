var URL = {
    userLogin: '/login',
    userLogout: '/logout',
    getSelfUserInfo: '/user/info/selfInfo',
    getUserInfoList: '/user/info/list',
    updateUserInfo: '/user/info/update',
    deleteUserInfo: '/user/info/delete',
    userAuthList: '/user/info/auth/list',
    updateUserAuth: '/user/info/auth/update',
    resetPassword: '/user/info/resetPassword',

    systemUpgradeInfo: '/system/info/upgrade',
};

var URL1 = {};

export default {
    URL, URL1
};




