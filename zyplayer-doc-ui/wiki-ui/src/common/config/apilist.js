var URL = {
    userLogin: '/login',
    userLogout: '/logout',
    getSelfUserInfo: '/user/info/selfInfo',
    pageUpdate: '/zyplayer-doc-wiki/page/update',
    pageChangeParent: '/zyplayer-doc-wiki/page/changeParent',
    pageList: '/zyplayer-doc-wiki/page/list',
    updatePage: '/zyplayer-doc-wiki/page/update',
    pageDetail: '/zyplayer-doc-wiki/page/detail',
    pageDelete: '/zyplayer-doc-wiki/page/delete',
    pageNews: '/zyplayer-doc-wiki/page/news',
    pageSearchByEs: '/zyplayer-doc-wiki/page/searchByEs',
    pageLock: '/zyplayer-doc-wiki/page/lock',
    pageUnlock: '/zyplayer-doc-wiki/page/unlock',
    spaceList: '/zyplayer-doc-wiki/space/list',
    updateSpace: '/zyplayer-doc-wiki/space/update',
    getPageUserAuthList: '/zyplayer-doc-wiki/page/auth/list',
    assignPageUserAuth: '/zyplayer-doc-wiki/page/auth/assign',

    updatePageFile: '/zyplayer-doc-wiki/page/file/update',
    pageCommentList: '/zyplayer-doc-wiki/page/comment/list',
    updatePageComment: '/zyplayer-doc-wiki/page/comment/update',
    pageZanList: '/zyplayer-doc-wiki/page/zan/list',
    updatePageZan: '/zyplayer-doc-wiki/page/zan/update',

    commonUpload: '/zyplayer-doc-wiki/common/upload',
    getUserBaseInfo: '/zyplayer-doc-wiki/common/user/base',

    systemUpgradeInfo: '/system/info/upgrade',
};

var URL1 = {};

export default {
    URL, URL1
};




